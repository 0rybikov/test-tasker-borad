// document.addEventListener('DOMContentLoaded', function() {
//     console.info('# DOCUMENT_LOAD_2');
// }, false);

// window.addEventListener('load', function () {
    console.info('# DOCUMENT_LOAD');

    // Задачник
    let tasks = {
        // allTasks - все задачи
        aProjects: [
            {
                id: '0',
                name: 'project-0'
            }
            ,{
                id: '1',
                name: 'project-1'
            }
            , {
                id: '2',
                name: 'project-2'
            }
            , {
                id: '3',
                name: 'project-3'
            }
        ]
        , aTestTasks: [
            {
                id: '1', // string
                title: 'Задача 1', // string
                idProject: '0', // string
                description: ' Таким образом начало повседневной работы по формированию позиции представляет собой интересный эксперимент проверки дальнейших направлений развития. Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности требуют определения и уточнения направлений прогрессивного развития. Товарищи! начало повседневной работы по формированию позиции требуют определения и уточнения направлений прогрессивного развития.',
                priority: 1 // number
            }
            , {
                id: '2', // string
                title: 'Задача 2', // string
                idProject: '0', // string
                description: ' Таким образом начало повседневной работы по формированию позиции представляет собой интересный эксперимент проверки дальнейших направлений развития. Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности требуют определения и уточнения направлений прогрессивного развития. Товарищи! начало повседневной работы по формированию позиции требуют определения и уточнения направлений прогрессивного развития.',
                priority: 3 // number
            }
            , {
                id: '3', // string
                title: 'Задача 3', // string
                idProject: '0', // string
                description: ' Таким образом начало повседневной работы по формированию позиции представляет собой интересный эксперимент проверки дальнейших направлений развития. Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности требуют определения и уточнения направлений прогрессивного развития. Товарищи! начало повседневной работы по формированию позиции требуют определения и уточнения направлений прогрессивного развития.',
                priority: 2 // number
            }
            ,{
                id: '4', // string
                title: 'Задача 4', // string
                idProject: '1', // string
                description: ' Таким образом начало повседневной работы по формированию позиции представляет собой интересный эксперимент проверки дальнейших направлений развития. Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности требуют определения и уточнения направлений прогрессивного развития. Товарищи! начало повседневной работы по формированию позиции требуют определения и уточнения направлений прогрессивного развития.',
                priority: 1 // number
            }
            , {
                id: '5', // string
                title: 'Задача 5', // string
                idProject: '1', // string
                description: ' Таким образом начало повседневной работы по формированию позиции представляет собой интересный эксперимент проверки дальнейших направлений развития. Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности требуют определения и уточнения направлений прогрессивного развития. Товарищи! начало повседневной работы по формированию позиции требуют определения и уточнения направлений прогрессивного развития.',
                priority: 3 // number
            }
            , {
                id: '6', // string
                title: 'Задача 6', // string
                idProject: '1', // string
                description: ' Таким образом начало повседневной работы по формированию позиции представляет собой интересный эксперимент проверки дальнейших направлений развития. Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности требуют определения и уточнения направлений прогрессивного развития. Товарищи! начало повседневной работы по формированию позиции требуют определения и уточнения направлений прогрессивного развития.',
                priority: 2 // number
            }
            ,{
                id: '7', // string
                title: 'Задача 7', // string
                idProject: '2', // string
                description: ' Таким образом начало повседневной работы по формированию позиции представляет собой интересный эксперимент проверки дальнейших направлений развития. Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности требуют определения и уточнения направлений прогрессивного развития. Товарищи! начало повседневной работы по формированию позиции требуют определения и уточнения направлений прогрессивного развития.',
                priority: 1 // number
            }
            , {
                id: '8', // string
                title: 'Задача 8', // string
                idProject: '2', // string
                description: ' Таким образом начало повседневной работы по формированию позиции представляет собой интересный эксперимент проверки дальнейших направлений развития. Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности требуют определения и уточнения направлений прогрессивного развития. Товарищи! начало повседневной работы по формированию позиции требуют определения и уточнения направлений прогрессивного развития.',
                priority: 3 // number
            }
            , {
                id: '9', // string
                title: 'Задача 9', // string
                idProject: '2', // string
                description: ' Таким образом начало повседневной работы по формированию позиции представляет собой интересный эксперимент проверки дальнейших направлений развития. Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности требуют определения и уточнения направлений прогрессивного развития. Товарищи! начало повседневной работы по формированию позиции требуют определения и уточнения направлений прогрессивного развития.',
                priority: 2 // number
            }
        ]
        , oCurrentTask: {} // хранилище добавления/редактирования задачи
        , sAliasValueAllProjectsBySelect: 'all'
        , sDataEmpty: 'Список пуст...'
        , aTasks: []
        , aViewTasks: []
        , jList: ''
        , jPanel: null
        , jPanelPriority: null
        , jPanelProjects: null
        , jPanelNew: null
        , jNewEditFormBox: null
        , jNewEditForm: null
        , sEditTask: ''
        , oParams: {
            projectId: 'all',
            isPriority: false
        }
        , init: function () { // инициализация
            console.log('#[TASKS] - INIT');
            tasks.jList = document.querySelector('.js-task-list');
            tasks.jPanel = document.querySelector('.js-panel');
            tasks.jPanelProjects = tasks.jPanel.querySelector('.js-panel__select-project');


            document.querySelector('.js-get-test-data__e').addEventListener('click', function (e) {
                e.target.style.display = "none";
                tasks.getTasksListTest();
                tasks.onResetForm();
            });

            tasks.generateDomListProjects();
            tasks.initEventsDefault();
        }
        , initEvents: function () {
            // события карточчки
            let  cMore = tasks.jList.querySelectorAll('.js-get-more');
            for(let cItemI = 0; cItemI < cMore.length; cItemI++){
                cMore[cItemI].removeEventListener('click', tasks.onViewMOre, false);
                cMore[cItemI].addEventListener('click', tasks.onViewMOre, false);
            }

            let  cDelete = tasks.jList.querySelectorAll('.js-delete-task');
            for(let cItemD = 0; cItemD < cDelete.length; cItemD++){
                cDelete[cItemD].removeEventListener('click', tasks.onDelete, false);
                cDelete[cItemD].addEventListener('click', tasks.onDelete, false);
            }

            let  cEdit = tasks.jList.querySelectorAll('.js-edit-task');
            for(let cItemE = 0; cItemE < cDelete.length; cItemE++){
                cEdit[cItemE].removeEventListener('click', tasks.onEdit, false);
                cEdit[cItemE].addEventListener('click', tasks.onEdit, false);
            }

            // END - события карточчки

            if(tasks.jPanel){
                // LISTENER by PROJECT - SELECT
                if(tasks.jPanelProjects){
                    tasks.jPanelProjects.removeEventListener('change', tasks.onFilterByProject, false);
                    tasks.jPanelProjects.addEventListener('change', tasks.onFilterByProject, false);
                }
                // LISTENER by PRIORITY - CHECKBOX
                tasks.jPanelPriority = tasks.jPanel.querySelector('.js-panel__order');
                if (tasks.jPanelPriority){
                    tasks.jPanelPriority.removeEventListener('change', tasks.onFilterByPriority, false);
                    tasks.jPanelPriority.addEventListener('change', tasks.onFilterByPriority, false);
                }

            }
        }
        , initEventsDefault: function () {
            // LISTENER by NEW TASK - BUTTON-div
            tasks.jPanelNew = tasks.jPanel.querySelector('.js-panel__add');
            if (tasks.jPanelNew){
                tasks.jPanelNew.removeEventListener('click', tasks.onAddOrEditTask, false);
                tasks.jPanelNew.addEventListener('click', tasks.onAddOrEditTask, false);
            }
        }
        , getTasksListTest: function(){ // Формируем HTML списка задач
            let cTestTasks = tasks.getTasksByTest();
            tasks.setDataOrig(cTestTasks);
            tasks.getData();
        }
        , getTasksByTest: function(){ // Получить задачи - все или только по проекту
            return tasks.aTestTasks;
        }
        , generateDomListProjects: function () { //
            tasks.jPanelProjects.innerHTML = tasks.generateProjectsDOM(tasks.aProjects);
        }

        // SETS_METHODS
        , setDataOrig: function(data) { // сохраняем данные
            if(data){
                tasks.aTasks = JSON.parse(JSON.stringify(data)); // перезаписываем без ссылок на объект;
                tasks.setLS('tasks', JSON.parse(JSON.stringify(data)));
            }
        }
        // LS - localstore

        , setLS: function(nameStore, data){
            // Проверить наличие данных в "localStorage"
            // typeItem - тип данных
            // idItem - ID данных
            if (nameStore && typeof(nameStore) === 'string' ) {
                // Проверяем наличие задачи
                localStorage.setItem(nameStore, JSON.stringify(data));
            } else {
                console.info('[TASKS] - Error Set DATA in LOCALSTORE');
            }
        }
        , checkLS: function(idTask){
            // Проверить наличие данных в "localStorage"
            // typeItem - тип данных
            // idItem - ID данных
            if (idTask && typeof(idTask) === 'string' ) {
                // Проверяем наличие задачи

            } else {
                // Проверяем, есть ли задачи
                if (JSON.parse(localStorage.getItem('tasks'))){

                }
                return null;
            }

        }
        , getDataFromLS: function(typeItem, idItem) {
            // получить данные из "localStorage".
            // typeItem - тип нужных данных
            // idItem - ID нужных данных
            if (typeItem && idItem) {

            } else {
                return null;
            }
        }
        // ##### END LS


        // RENDERer
        , generateTasksDOM: function(aTasks){ // сформировать вёрстку для списка задач. aTasks - список задач
            let cHTML = '';
            if (aTasks && aTasks.length){
                for( let cItemJ=0; cItemJ < aTasks.length; cItemJ++){
                    cHTML+=tasks.generateTaskDOM(aTasks[cItemJ]);
                }
            }
            tasks.jList.innerHTML = cHTML;
        }
        , generateTaskDOM: function(iTask) { // сформировать вёрстку для списка задач. aTasks - список задач
            let cHTML = '<div class="or-tasks__list-i"><div class="or-tasks-item js-task-item"><div class="or-tasks-item__inner">'
                + '<div class="or-tasks-item__title">'
                + (iTask.title || 'Без названия')
                + '</div><div class="or-tasks-item__info"><div class="or-tasks-item__info-cell _flex">'
                + '<span class="_gray">Проект:&nbsp;</span> <b>'
                + tasks.getNameProject(iTask.idProject)
                + '</b></div>'
                + '<div class="or-tasks-item__info-cell"><span class="_gray">' + 'Приоритет:&nbsp;</span> <b>'
                + (iTask.priority || 0)
                + '</b> </div>'
                + '<div class="or-tasks-item__info-cell _wide"> <div class="or-tasks-item-desc">'
                +  (iTask.description || 'Описание отсутствует')
                + '</div></div></div>'
                + '<div class="or-tasks-item__nav"><div class="or-form"><div class="or-form__row _coll-3">'
                + '<div class="or-form__element _button js-edit-task" data-id="' + iTask.id + '">'
                + 'ИЗМЕНИТЬ'
                + '</div><div class="or-form__element _button js-delete-task" data-id="' + iTask.id + '">'
                + 'ЗАКРЫТЬ'
                + '</div><div class="or-form__element _button-link _d-text">'
                + '<div style="display: inline" class="js-get-more">'
                + '<span class="_d-text--start ">РАЗВЕРНУТЬ</span><span class="_d-text--result">Свернуть</span>'
                + '</div>'
                + '</div></div></div></div></div></div></div>';
            return cHTML;
        }
        , generateDOMEmptyMSG: function(sText) { // сформировать контейнер - сообщение, если данных нет
            return '<span class="or-tasks-msg">' + sText + '</span>'
        }
        , generateProjectsDOM: function(aProjects) { // сформировать вёрстку для списка проектов в панели управления
            let cHTML ='<option '+(tasks.oParams.projectId === 'all' ? 'selected' : '')+' value="all">Все</option>';
            if(aProjects && aProjects.length){
                for (let i = 0; i < aProjects.length ; i++) {
                    cHTML+= '<option value="'+aProjects[i].id+'" '+ (tasks.oParams.projectId === aProjects[i].id ? 'selected' : '') +' >' + aProjects[i].name + '</option>';
                }
            }
            return cHTML;
        }
        , getNameProject: function (idProject) { // Получаем имя проекта
            let cProject = tasks.aProjects.filter( function (item) { return item.id === idProject; } );
            return (cProject.length) ? cProject[0].name: 'отсутствует';
        }
        , deleteTask: function(idTask){ //Удаление задачи
            if ((idTask && typeof(idTask) === 'string')){
                tasks.aTasks = tasks.aTasks.filter(function (item) {
                    return item.id !== idTask;
                })
            }
            // tasks.setLS('tasks', tasks.aTasks);
            tasks.getData();
        }

        , updateList: function(aTasks, isOnlyRender){ // Обновляем вьюху со списком. isOnlyRender - если только обновить список, без сохранения данных
            if(aTasks){
                tasks.aViewTasks = aTasks;
                tasks.setLS('tasksView', tasks.aViewTasks);
                tasks.generateTasksDOM(aTasks);
                if (!aTasks.length){
                    tasks.jList.innerHTML = tasks.generateDOMEmptyMSG(tasks.sDataEmpty);
                    tasks.lockControls(true);
                } else{
                    tasks.initEvents();
                    tasks.lockControls(false);
                }
            }
        }
        , lockControls: function (isLock) { // Блокировка контролов
            if(isLock){
                tasks.jPanelPriority.disabled = true;
                if(!tasks.aTasks.length){
                    tasks.jPanelProjects.disabled = true;
                }
                //TODO - пока не допилин функционал добавления задач - кнопка задизейблена
                // if(tasks.jPanelNew.classList){
                //     if(!tasks.jPanelNew.classList.contains('_disabled')){ tasks.jPanelNew.classList.add('_disabled') }
                // }
            } else{
                tasks.jPanelPriority.disabled = false;
                tasks.jPanelProjects.disabled = false;
                //TODO - пока не допилин функционал добавления задач - кнопка задизейблена
                // if(tasks.jPanelNew.classList){
                //     if(tasks.jPanelNew.classList.contains('_disabled')){ tasks.jPanelNew.classList.remove('_disabled') }
                // }
            }

        }
        // #### END


        // PANEL - METHODS
        ,filterData: function(field, value, data){ // фильтрация списка
            let fData = JSON.parse(JSON.stringify(data)); // дублируем исходные данные для фильтрации, минус ссылка на исходник
            let cResult = [];
            if((field && typeof(field) === 'string') && (value && typeof(value) === 'string')){
                if(fData && fData.length){
                    if(value === tasks.sAliasValueAllProjectsBySelect){ // если выбрано не уникальное значение
                        cResult = data;
                    } else{
                        cResult =  fData.filter(function (item) {
                            if (item[field]){
                                return item[field] === value;
                            }
                        });
                    }
                }
            }
            return cResult;
        }
        ,sortData: function(field, typeSort, data){ // сортировка списка
            let fData = JSON.parse(JSON.stringify(data)); // дублируем исходные данные для фильтрации, минус ссылка на исходник
            let cResult = []; // результирующий массив
            if((field && typeof(field) === 'string') && (typeSort && typeof(typeSort) === 'string')){
                if(fData && fData.length){
                    cResult = fData.sort(function(a, b) {
                        if(a[field] && b[field]){
                            let c = '';
                            let d = '';
                            if(typeof (a[field]) === 'string' || typeof ([field]) === 'string' ){
                                 c = JSON.stringify(a[field]).toLowerCase().replace(/\s/g, '');
                                 d = JSON.stringify(b[field]).toLowerCase().replace(/\s/g, '');
                            } else{
                                if(typeof (a[field]) === 'number' && typeof (b[field]) === 'number' ){
                                     c = a[field];
                                     d = b[field];
                                }
                            }
                            return tasks.sortAscDesc(typeSort, c, d);
                        }
                    });
                } else {  cResult = JSON.parse(JSON.stringify(data)) }
            }
            return cResult;
        }
        ,getData: function(){ // Получаем данные и обновляем список
            console.info('#[TASKS] - Get Data !');
            let cResult = [];
            if(tasks.aTasks.length){
                cResult =  tasks.filterData('idProject', tasks.oParams.projectId, tasks.aTasks);
                if (tasks.oParams.isPriority){ // если активен флаг приоритета
                    cResult = tasks.sortData('priority', 'ASC', cResult) ;
                }
            }
            tasks.setLS('tasks', tasks.aTasks);
            tasks.updateList(cResult);
        }
        ,initEventNewEdit: function () { // Инициализаця слушателей на форме добавления
            if(tasks.jNewEditForm){
                tasks.jNewEditForm.querySelector('input[name="task-save"]').removeEventListener('click', tasks.onValidate, false);
                tasks.jNewEditForm.querySelector('input[name="task-save"]').addEventListener('click', tasks.onValidate, false);

                tasks.jNewEditForm.removeEventListener('submit', tasks.onSaveData, false);
                tasks.jNewEditForm.addEventListener('submit', tasks.onSaveData, false);

                tasks.jNewEditForm.querySelector('input[name="task-reset"]').removeEventListener('click', tasks.onResetForm, false);
                tasks.jNewEditForm.querySelector('input[name="task-reset"]').addEventListener('click', tasks.onResetForm, false);
            }
        }
        , setPanelStatus: function (sStatus) { // Устанавливаем статус для панели. sStatus - css-класс для отображения формы состояния
            if (tasks.jPanel && sStatus && typeof (sStatus) === 'string') {
                tasks.jPanel.classList.add(sStatus);
            }
        }
        , clearPanelStatus: function () { // Обнуляем статус у панели
            if (tasks.jPanel) {
                tasks.jPanel.classList.remove('_main');
                tasks.jPanel.classList.remove('_add-edit');
                if(tasks.jNewEditFormBox.classList){
                    tasks.jNewEditFormBox.classList.remove('_validate');
                }

            }
        }
        ,addTask: function(taskData){ // добавление задачи в хранилище
            let cResultTasks = JSON.parse(JSON.stringify(tasks.aTasks));
            let cTaskOld = cResultTasks.filter(function (item) { return item.id === taskData.id });

            if (cTaskOld.length){ // если запись уже существует, то обновляем её
                // let cItem = cResultTasks.find(function (item) { return item.id === taskData.id });
                // for (let keyObj in cItem) {
                //     if(taskData[keyObj]){cItem[keyObj]=taskData[keyObj]}
                // }

                // IE - 11
                cResultTasks.map(function (item) {
                    if(item.id === taskData.id){
                        for (let keyObj in item) {
                            if (taskData[keyObj]) { item[keyObj]=taskData[keyObj]; }
                        }
                    }
                })


            } else{ // либо добавляем новую в конец массива
                cResultTasks.push(taskData);
            }

            tasks.cFormReset();
            tasks.clearPanelStatus();
            tasks.setDataOrig(cResultTasks);
            tasks.generateDomListProjects();
            tasks.getData();
        }
        , editTask: function(idTask){ //Изменение задачи
            let cResult = [];
            if ((idTask && typeof(idTask) === 'string')){
                let cDataEditTask = tasks.aTasks.filter(function (item) {
                    return item.id === idTask;
                });
                // Если информация по задаче найдена
                if(cDataEditTask.length){
                    tasks.sEditTask = idTask;
                    tasks.onAddOrEditTask();
                    if(tasks.jNewEditForm.elements['task-name']){ tasks.jNewEditForm.elements['task-name'].value = cDataEditTask[0].title;}
                    if(tasks.jNewEditForm.elements['task-name']){ tasks.jNewEditForm.elements['task-project'].value = tasks.getNameProject(cDataEditTask[0].idProject);}
                    if(tasks.jNewEditForm.elements['task-name']){ tasks.jNewEditForm.elements['task-priority'].value = cDataEditTask[0].priority; }
                    if(tasks.jNewEditForm.elements['task-name']){ tasks.jNewEditForm.elements['task-desc'].value = cDataEditTask[0].description; }
                }
            }
            //
            // tasks.getData();
        }
        , cFormReset: function () { // Обнуляем форму
            if (tasks.jPanel) {
                if(tasks.jNewEditForm){ tasks.jNewEditForm.reset()};
            }
        }
        , checkProject: function(sProjectName) { // Проверяем существует ли такой проект
            return tasks.aProjects.filter(function (item) {
                return  item.name === sProjectName;
            })
        }

        // CARD onMETHODS
        , onViewMOre: function(e){ // РАЗВЕРНУТЬ
            let cMoreI = e.target;
            let cParent = tasks.closestBox(cMoreI, '.js-task-item');
            if(cParent){ cParent.classList.toggle('_desc'); }
        }
        , onDelete: function(e){ // УДАЛЕНИЕ задачи из списка
            console.info('#[TASKS] - Delete TASK');
            tasks.onResetForm();
            let cMoreI = e.target;
            let cId = (cMoreI.dataset) ? ((cMoreI.dataset.id)?cMoreI.dataset.id:null):null;
            cId && typeof(cId) === 'string' ? tasks.deleteTask(cId) : console.info('#[TASKS] - Not Found Task');
        }
        , onEdit: function (e) {
            console.info('#[TASKS] - Open Form EDIT TASK');
            tasks.onResetForm();
            let cMoreI = e.target;
            let cId = (cMoreI.dataset) ? ((cMoreI.dataset.id)?cMoreI.dataset.id:null):null;
            cId && typeof(cId) === 'string' ? tasks.editTask(cId) : console.info('#[TASKS] - Not Found Task');
        }
        // PANEL onMETHODS
        , onFilterByProject: function (e) {
            console.info('#[TASKS] - Filtering By Project');
            let cMoreI = e.target;
            tasks.oParams.projectId = cMoreI.value;
            tasks.getData();
        }
        , onFilterByPriority: function (e) {
            console.info('#[TASKS] - Filtering By Priority');
            let cMoreI = e.target;
            tasks.oParams.isPriority = cMoreI.checked;
            tasks.getData();
        }
        , onAddOrEditTask: function () { // idTask - если редактируем запись
            console.info('#[TASKS] - Add|Edit  Task');
            tasks.jNewEditFormBox = document.querySelector('.js-panel__form-add');
            tasks.jNewEditForm = document.querySelector('.js-panel__form-add').querySelector('.js-form');
            tasks.clearPanelStatus();
            tasks.setPanelStatus('_add-edit');
            tasks.initEventNewEdit();
        }
        ,onSaveData: function(e) { // Нажимаем "СОХРАНИТЬ"
            console.info('#[TASKS] - Save Task');
            e.preventDefault();
            let cForm = e.target;
            if (cForm) {
                let cFormElems = cForm.elements;
                if (
                    cFormElems['task-name']
                    && cFormElems['task-project']
                    && cFormElems['task-priority']
                    && cFormElems['task-desc']) {
                    let cNewTask = {
                        id: (tasks.sEditTask)? tasks.sEditTask: JSON.stringify(Date.now()), // id - если не редактирование, то random - DATE now =)
                        title: cFormElems['task-name'].value, // string
                        idProject: JSON.stringify(tasks.aProjects.length), // string
                        description: cFormElems['task-desc'].value,
                        priority: parseInt(cFormElems['task-priority'].value) // number
                    };

                    let cProject = cFormElems['task-project'].value;
                    let cProjectCheck = tasks.checkProject(cProject);
                    if (cProjectCheck.length) {
                        cNewTask.idProject = cProjectCheck[0].id;
                    } else{
                        tasks.aProjects.push({
                            id: JSON.stringify(tasks.aProjects.length),
                            name: cFormElems['task-project'].value
                        });
                    }
                    tasks.addTask(cNewTask);
                }
            }
        }
        ,onValidate: function(){ // подсветка формы, если поля не заполнены
            if(tasks.jNewEditFormBox.classList){
                if (!tasks.jNewEditFormBox.classList.contains('_validate')){
                    tasks.jNewEditFormBox.classList.add('_validate');
                }
            }
        }
        ,onResetForm: function(){ // сбрасываем форму и состояние
            if (tasks.jNewEditForm){
                tasks.cFormReset();
                tasks.clearPanelStatus();
            }
        }


        // help methods
        , closestBox: function(el, selector) { // Поиск родителя элемента
            let matchesFn;

            // find vendor prefix
            ['matches', 'webkitMatchesSelector', 'mozMatchesSelector', 'msMatchesSelector', 'oMatchesSelector'].some(function(fn) {
                if (typeof document.body[fn] == 'function') {
                    matchesFn = fn;
                    return true;
                }
                return false;
            });

            let parent;

            // traverse parents
            while (el) {
                parent = el.parentElement;
                if (parent && parent[matchesFn](selector)) {
                    return parent;
                }
                el = parent;
            }

            return null;
        }
        , sortAscDesc: function(typeSort, c, d){ // метод сортировки c, d по типу "typeSort"
            if(typeSort === 'ASC'){
                if (c > d) {
                    return 1;
                }
                if (c < d) {
                    return -1;
                }
                return 0;
            }
            if(typeSort === 'DESC'){
                if (c < d) {
                    return 1;
                }
                if (c > d) {
                    return -1;
                }
                return 0;
            }
        }
    };

    // RUN TASKS
    if(document.querySelector('.js-task')){
        tasks.init();
    } else {
        console.info('#[TASKS] - NOT INIT');
    }
// });